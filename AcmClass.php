<?php

if( !defined( 'MEDIAWIKI' ) )
	die( -1 );

/**
 * Inherit main code from SkinTemplate, set the CSS and template filter.
 * @todo document
 * @ingroup Skins
 */
class SkinAcmClass extends SkinTemplate {
	var $skinname = 'acmclass', $stylename = 'acmclass',
		$template = 'AcmClassTemplate', $useHeadElement = false;

	/**
	 * @param $out OutputPage
	 */
	function setupSkinUserCss( OutputPage $out ) {
	}
}

/**
 * @todo document
 * @ingroup Skins
 */
class AcmClassTemplate extends BaseTemplate {

	/**
	 * Template filter callback for AcmClass skin.
	 * Takes an associative array of data set from a SkinTemplate-based
	 * class, and a wrapper for MediaWiki's localization database, and
	 * outputs a formatted page.
	 *
	 * @access private
	 */
	function execute() {
		// Suppress warnings to prevent notices about missing indexes in $this->data
		wfSuppressWarnings();
		global $wgStylePath;
?><!DOCTYPE html>
<html>
<head>
  <title>上海交通大学ACM班 - <?php $this->html( 'title' ); ?></title>
  <meta charset="UTF-8"/>
  <link href="<?php echo $wgStylePath; ?>/acmclass/main.css" media="screen" rel="stylesheet" type="text/css" />
  <link href="<?php echo $wgStylePath; ?>/acmclass/topic_nav.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="main">
  <div id="heading">
    <div class="inner">
      <h1>
        <a href="/">
          <img alt="Head-logo" src="http://acm.sjtu.edu.cn/themes/acm_class/images/head-logo.png" title="上海交通大学ACM班" />
        </a>
      </h1>
      <div id="nav" class="nav">
        <div class="navlist">
          <ul class="nav">
            <li><a href="/">首页</a></li>
            <li><a href="/班级介绍">班级介绍</a></li>
            <li><a href="/招生信息">招生信息</a></li>
            <li class="active"><a href="http://acm.sjtu.edu.cn/wiki/课程中心">课程中心</a></li>
            <li><a href="http://acm.sjtu.edu.cn/wiki/科研成果">科研成果</a></li>
            <li><a href="/课余生活">课余生活</a></li>
            <li><a href="/icpc">ACM竞赛</a></li>
          </ul>
        </div><!-- END .navlist -->
      </div><!-- END #nav -->
    </div><!-- END .inner -->
  </div><!-- END #heading -->
  <div class="subpage" id="content">
    <div class="inner-page">
      <div class="newspaper">
        <div id="blog-post">
          <div class="post-content">
            <div class="zh_page_body mediawiki">
<div id="mw-js-message" style="display:none;"></div>
<?php if ( $this->data['newtalk'] ) { ?><div class="usermessage"><?php $this->html( 'newtalk' ); ?></div><?php } ?>
<?php if ( $this->data['sitenotice'] ) { ?><div id="siteNotice"><?php $this->html( 'sitenotice' ); ?></div><?php } ?>
<h2 id='page-title'><?php $this->html( 'title' ); ?></h2>
<?php $this->html( 'bodytext' ) ?>
            </div>
          </div>
          <div class="post-meta">
            <div class="topic_nav">
<?php foreach ( $this->getSidebar() as $boxName => $box ) { ?>
	<div id='<?php echo Sanitizer::escapeId( $box['id'] ) ?>'<?php echo Linker::tooltip( $box['id'] ) ?>>
		<p class="vspace sidehead"><a class="wikilink"><?php echo htmlspecialchars( $box['header'] ); ?></a></p>
	<?php if ( is_array( $box['content'] ) ) { ?>
		<ul>
		<?php foreach ( $box['content'] as $key => $item ) { ?>
			<?php echo $this->makeListItem( $key, $item ); ?>
		<?php } ?>
		</ul>
	<?php } else { ?>
		<?php echo $box['content']; ?>
	<?php } ?>
	</div>
<?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!-- END #main -->
<div id="footer">
  <p class="part-1">
	Powered by MediaWiki
	<?php foreach ( $this->data['content_actions'] as $key => $tab ) { ?>
	 	| <?php echo $this->makeLink( $key, $tab ); ?>
	<?php } ?>
	<?php global $wgUser; ?>
	<?php if ($wgUser->isLoggedIn()) { ?>
	| <a href="http://acm.sjtu.edu.cn/wiki/Special:UserLogout">Logout</a>
	<?php } else { ?>
	| <a href="http://acm.sjtu.edu.cn/wiki/Special:UserLogin">Login</a>
	<?php } ?>
  </p>
  <p class="part-2">
    Copyright &copy; <?php echo date('Y'); ?> <a href="http://acm.sjtu.edu.cn/">ACM Class</a>. All rights reserved.
  </p>
</div>
<?php
		$this->printTrail();
		echo Html::closeElement( 'body' );
		echo Html::closeElement( 'html' );
		wfRestoreWarnings();
	} // end of execute() method
} // end of class
