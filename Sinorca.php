<?php

if( !defined( 'MEDIAWIKI' ) )
	die( -1 );

/**
 * Inherit main code from SkinTemplate, set the CSS and template filter.
 * @todo document
 * @ingroup Skins
 */
class SkinSinorca extends SkinTemplate {
	/** Using sinorca. */
	var $skinname = 'sinorca', $stylename = 'sinorca',
		$template = 'SinorcaTemplate', $useHeadElement = false;

	/**
	 * @param $out OutputPage
	 */
	function setupSkinUserCss( OutputPage $out ) {
	}
}

/**
 * @todo document
 * @ingroup Skins
 */
class SinorcaTemplate extends BaseTemplate {

	/**
	 * Template filter callback for Sinorca skin.
	 * Takes an associative array of data set from a SkinTemplate-based
	 * class, and a wrapper for MediaWiki's localization database, and
	 * outputs a formatted page.
	 *
	 * @access private
	 */
	function execute() {
		// Suppress warnings to prevent notices about missing indexes in $this->data
		wfSuppressWarnings();
		global $wgStylePath;
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="$[en]" lang="$[en]">

<head>

<title><?php $this->html( 'title' ); ?> - <?php $this->text( 'sitename' ); ?></title>

<style rel='stylesheet' type='text/css' media='screen, projection'/>
	@import url(<?php echo $wgStylePath; ?>/sinorca/basic.css);
	@import url(<?php echo $wgStylePath; ?>/sinorca/layout.css);
	@import url(<?php echo $wgStylePath; ?>/sinorca/sinorca.css);
</style>

<!--[if IE 5.5]>
<link rel='stylesheet' type='text/css' media='screen, projection' href='<?php echo $wgStylePath; ?>/sinorca/sinorca-ie-5.5.css' />
<![endif]-->

<!--[if IE 6]>
<link rel='stylesheet' type='text/css' media='screen, projection' href='<?php echo $wgStylePath; ?>/sinorca/sinorca-ie-6.0.css' />
<![endif]-->

<!--HTMLHeader-->

<link rel='stylesheet' type='text/css'  media='screen' href='<?php echo $wgStylePath; ?>/sinorca/fixed.css' />

</head>

<body>

<div id='wiki-wrap' class='wiki-wrap'>

<!--PageTopFmt-->
<div id='wiki-top' class='wiki-top'>
	<div id="top-left"></div>
	<div id="top-right"><ul>
	<?php foreach ( $this->getPersonalTools() as $key => $item ) { ?>
		<?php echo $this->makeListItem($key, $item); ?>
	<?php } ?>
	</ul></div>
</div>
<!--/PageTopFmt-->

<!--PageHeadFmt-->
<div id='wiki-head' class='wiki-head'>
	<h1><a href="<?php echo $this->data['nav_urls']['mainpage']['href']; ?>"><img src="http://acm.sjtu.edu.cn/themes/acm_class_homepage/images/head-logo.png"/></a></h1>
	<form  action='<?php $this->text( 'wgScript' ); ?>' method='get'>
		<input type='hidden' name="title" value="<?php $this->text( 'searchtitle' ) ?>" />
		<input type='search' name='search' value='' placeholder='Search' size='17' />
		<input type='submit' name='go' value='Go'/>
		<input type='submit' name='fulltext' value='Search'/>
	</form>
</div>
<!--/PageHeadFmt-->

<!--PageInfoFmt-->
<div id='wiki-info' class='wiki-info'>
	<div id="info-left"></div>
	<div id="info-right"><ul>
	<?php foreach ( $this->data['content_actions'] as $key => $tab ) { ?>
		<?php echo $this->makeListItem( $key, $tab ); ?>
	<?php } ?>
	</ul></div>
</div>
<!--/PageInfoFmt-->

<!--PageMenuFmt-->
<div id='wiki-menu' class='wiki-menu'>
<?php foreach ( $this->getSidebar() as $boxName => $box ) { ?>
	<div id='<?php echo Sanitizer::escapeId( $box['id'] ) ?>'<?php echo Linker::tooltip( $box['id'] ) ?>>
		<p class="vspace sidehead"><a class="wikilink"><?php echo htmlspecialchars( $box['header'] ); ?></a></p>
	<?php if ( is_array( $box['content'] ) ) { ?>
		<ul>
		<?php foreach ( $box['content'] as $key => $item ) { ?>
			<?php echo $this->makeListItem( $key, $item ); ?>
		<?php } ?>
		</ul>
	<?php } else { ?>
		<?php echo $box['content']; ?>
	<?php } ?>
	</div>
<?php } ?>
</div>
<!--/PageMenuFmt-->

<div id='wiki-page' class='wiki-page'>
	<div id="mw-js-message" style="display:none;"></div>
	<?php if ( $this->data['newtalk'] ) { ?><div class="usermessage"><?php $this->html( 'newtalk' ); ?></div><?php } ?>
	<?php if ( $this->data['sitenotice'] ) { ?><div id="siteNotice"><?php $this->html( 'sitenotice' ); ?></div><?php } ?>
	<h2 id='page-title'><?php $this->html( 'title' ); ?></h2>
	<?php $this->html( 'bodytext' ) ?>
	<hr id='page-end'/>
</div>

<!--PageFootFmt-->
<div id='wiki-foot' class='wiki-foot'>
	<div id="foot-left">
	<?php
	foreach ( $this->getFooterLinks() as $category => $links ) { ?>
	<ul>
	<?php
		foreach ( $links as $key ) { ?>
		<li><?php $this->html( $key ) ?></li>

	<?php
		} ?>
	</ul>
	<?php
	} ?>
	</div>
	<div id="foot-right">
		<ul>
		<?php
			foreach ( $this->getFooterIcons( "icononly" ) as $blockName => $footerIcons ) { ?>
			<li>
		<?php
				foreach ( $footerIcons as $icon ) { ?>
				<?php echo $this->getSkin()->makeFooterIcon( $icon ); ?>

		<?php
				} ?>
			</li>
		<?php
			} ?>
		</ul>
	</div>
</div>
<!--/PageFootFmt-->

</div>
<?php
		$this->printTrail();
		echo Html::closeElement( 'body' );
		echo Html::closeElement( 'html' );
		wfRestoreWarnings();
	} // end of execute() method
} // end of class
